
/**

 *  functionality: Uses volley to connect and receive data from node.js server

 *  created:       2016-04-20

 *  @author:       Andrew Pleas

 **/

package com.example.apleas.vtest;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


public class MainActivity extends AppCompatActivity  {

    public static final String JSON_URL = "http://10.0.2.2:8080";



    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listView);
        sendRequest();

    }
    /**

     *  functionality: sends a string request to node.js using a volley method stringRequest.  The response
     is then checked to make sure it is a string.  If it is not an error is thrown.  If it is a String
     The response is sent to showJSON.

     *  @author: Andrew Pleas

     *  @exception:  onErrorResponse method is called if there is an error

     **/
    public void sendRequest(){
        StringRequest stringRequest = new StringRequest(JSON_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showJSON(response);

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this,error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    /**

     *  functionality: sends the json string response to ParseJSON.
     It then sends all of the string arrays created by ParseJSON to CustomList

     *  @author: Andrew Pleas
     *  @param: json (Date) String
     *  @exception:  onErrorResponse method is called if there is an error

     **/
    private void showJSON(String json){
        ParseJSON pj = new ParseJSON(json);
        pj.parseJSON();
        CustomList cl = new CustomList(this, ParseJSON.days,
                ParseJSON.months, ParseJSON.years, ParseJSON.hrs,ParseJSON.leagues ,ParseJSON.groups, ParseJSON.scores,
                ParseJSON.homeNames, ParseJSON.homeImages, ParseJSON.oponentNames , ParseJSON.oponentImages, ParseJSON.tournaments);
        listView.setAdapter(cl);
    }


}