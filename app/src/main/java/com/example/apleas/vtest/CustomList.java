/**

 *  functionality: This class creates a listview from the array lists created in ParseJSON

 *  created:       2016-04-20

 *  @author:       Andrew Pleas

 Method getView sets fonts, checks if the the game is a tournament if it is it changes the color of the appropriate layout.
 Sets the data of each view in a row using the data from ass theociated arrayL list.

 **/

package com.example.apleas.vtest;

import android.app.Activity;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Locale;


public class CustomList extends ArrayAdapter<String> {
    private String[] days;
    private String[] months;
    private String[] years;
    private String[] hrs;
    private String[] leagues;
    private String[] groups;
    private String[] scores;
    private String[] homeNames;
    private String[] homeImages;
    private String[] opponentNames;
    private String[] opponentImages;
    private String[] tournaments;
    private Activity context;

    /**

     *  functionality: takes all of the String Arrays created in ParseJSON and creates a list view.

     *  @author: Andrew Pleas
     *  @param: Activity (Date) context, (days, months, years, hrs, leagues, groups, scores, homeNames, homeImages, opponentNames, opponentImages, tournament (Data) String[])
     *  @exception:  onErrorResponse method is called if there is an error
     *  @return a listViewItem
     **/

    public CustomList(Activity context, String[] days, String[] months, String[] years, String[] hrs, String[] leagues, String[] groups,
                      String[] scores,String[] homeNames, String[] homeImages,String[] opponentNames, String[] opponentImages, String[] tournaments) {
        super(context, R.layout.list_view_layout, days);
        this.context = context;
        this.days = days;
        this.months = months;
        this.years = years;
        this.hrs = hrs;
        this.leagues = leagues;
        this.groups = groups;
        this.scores = scores;
        this.homeNames = homeNames;
        this.homeImages = homeImages;
        this.opponentNames = opponentNames;
        this.opponentImages = opponentImages;
        this.tournaments = tournaments;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.list_view_layout, null, true);
        //creates an AssetManager to set variables for fonts
        AssetManager am = context.getApplicationContext().getAssets();
        Typeface rRegular = Typeface.createFromAsset(am,
                String.format(Locale.US, "fonts/%s", "Roboto-Regular.ttf"));
        Typeface rLight = Typeface.createFromAsset(am,
                String.format(Locale.US, "fonts/%s", "Roboto-Light.ttf"));
        Typeface rBlack = Typeface.createFromAsset(am,
                String.format(Locale.US, "fonts/%s", "Roboto-Black.ttf"));

        //The color of the view l is changed ig the game is a tournament
        LinearLayout l = (LinearLayout) listViewItem.findViewById(R.id.gameType);
        if(tournaments[position].equals("yes")){
            l.setBackgroundColor(Color.parseColor("#a21153"));
        }else{
            l.setBackgroundColor(Color.parseColor("#66ffcc"));
        }
        //Creates new TextViews and ImageViews. Sets the text, font and font size for text views
        TextView dId = (TextView) listViewItem.findViewById(R.id.day);
        dId.setTypeface(rRegular);
        dId.setTextSize(20);
        dId.setTextColor(Color.parseColor("#ffffff"));

        TextView mId = (TextView) listViewItem.findViewById(R.id.month);
        mId.setTypeface(rRegular);
        mId.setTextSize(13);
        mId.setTextColor(Color.parseColor("#ffffff"));

        TextView yId = (TextView) listViewItem.findViewById(R.id.year);
        yId.setTypeface(rLight);
        yId.setTextSize(11);
        yId.setTextColor(Color.parseColor("#ffffff"));

        TextView hId = (TextView) listViewItem.findViewById(R.id.hr);
        hId.setTypeface(rRegular);
        hId.setTextSize(11);
        hId.setTextColor(Color.parseColor("#ffffff"));


        ImageView hImage = (ImageView) listViewItem.findViewById(R.id.homeImage);

        TextView homeId = (TextView) listViewItem.findViewById(R.id.home);
        homeId.setTypeface(rBlack);
        homeId.setTextSize(12);

        TextView leagueView = (TextView) listViewItem.findViewById(R.id.league);
        leagueView.setTypeface(rRegular);
        leagueView.setTextSize(10);

        TextView gId = (TextView) listViewItem.findViewById(R.id.group);
        gId.setTypeface(rRegular);
     //   gId.setTextColor(696969);
        gId.setTextSize(10);

        TextView sId = (TextView) listViewItem.findViewById(R.id.score);
        sId.setTypeface(rBlack);
        sId.setTextSize(21);


        ImageView oImage = (ImageView) listViewItem.findViewById(R.id.opponentImage);
        TextView oId = (TextView) listViewItem.findViewById(R.id.opponent);
        oId.setTypeface(rBlack);
        oId.setTextSize(12);

        //Gets the int resource of the drawable onject for the home team and opponent teams logos using the
        //string of the position in the appropriate array so a variable can be used to set the image.
        String hometeam = homeImages[position];
        int home = context.getResources().getIdentifier(hometeam, "drawable", "com.example.apleas.vtest");;
        String oppteam = opponentImages[position];
        int opp = context.getResources().getIdentifier(oppteam, "drawable", "com.example.apleas.vtest");;

        //sets the views with the appropriate data
        homeId.setText(homeNames[position]);
        hImage.setImageResource(home);
        dId.setText(days[position]);
        mId.setText(months[position]);
        yId.setText(years[position]);
        hId.setText(hrs[position]);
        leagueView.setText(leagues[position]);
        gId.setText(groups[position]);
        sId.setText(scores[position]);
        oImage.setImageResource(opp);
        oId.setText(opponentNames[position]);

        return listViewItem;
    }
}