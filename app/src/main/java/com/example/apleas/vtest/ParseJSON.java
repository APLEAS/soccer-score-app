
/**

 *  functionality: Parses a json string to be used in a list view.

 *  created:       2016-04-20

 *  @author:       Andrew Pleas

 **/

package com.example.apleas.vtest;


import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ParseJSON {
    public static String[] days;
    public static String[] months;
    public static String[] years;
    public static String[] hrs;
    public static String[] leagues;
    public static String[] groups;
    public static String[] scores;
    public static String[] homeImages;
    public static String[] homeNames;
    public static String[] oponentImages;
    public static String[] oponentNames;
    public static String[] tournaments;


    public static final String KEY_DAY = "day";
    public static final String KEY_MONTH = "month";
    public static final String KEY_YEAR = "year";
    public static final String KEY_HR = "hr";
    public static final String KEY_LEAGUE = "league";
    public static final String KEY_GROUP = "group";
    public static final String KEY_SCORE = "score";
    public static final String KEY_HIMAGE = "himage";
    public static final String KEY_HOME = "home";
    public static final String KEY_OIMAGE = "oimage";
    public static final String KEY_OPONENT = "opponent";
    public static final String KEY_TOURNAMENT = "tournament";

    private JSONArray schedule = null;
    private String json;
    public ParseJSON(String json){
        this.json = json;
    }

    /**

     *  functionality: accepts a string as a parameter.  It takes the string and conerts it to a
     json array by creating a new volley JSONArray and passing the json string as an argument.  The
     array passed in contains json objects.  The json array is then iterated using a for loop.  A
     json object is created by creating a new JSONObject and passing the the ith element of the array
     as the argument.  For each json object in the array the value is extracted using the key and stored
     in the appropriate string array.  These arrays are public so they can be accesses outside of this class.

     *  @author: Andrew Pleas

     *  @param: json (Date) String

     *  @exception: JSONException e

     **/
    protected void parseJSON(){

        try{
            Log.d("this is my string", "arr: " + json);

            schedule = new JSONArray(json);

            Log.d("this is my array length", "arr: " + schedule.length());

            days = new String[schedule.length()];
            months = new String[schedule.length()];
            years = new String[schedule.length()];

            hrs = new String[schedule.length()];
            leagues = new String[schedule.length()];
            groups = new String[schedule.length()];
            scores = new String[schedule.length()];
            homeNames = new String[schedule.length()];
            homeImages = new String[schedule.length()];
            oponentImages = new String[schedule.length()];
            oponentNames = new String[schedule.length()];
            tournaments = new String[schedule.length()];

            for(int i=0;i<schedule.length();i++) {
                JSONObject jo = schedule.getJSONObject(i);
                days[i] = jo.getString(KEY_DAY);
                months[i] = jo.getString(KEY_MONTH);
                years[i] = jo.getString(KEY_YEAR);
                hrs[i] = jo.getString(KEY_HR);
                leagues[i] = jo.getString(KEY_LEAGUE);
                groups[i] = jo.getString(KEY_GROUP);
                scores[i] = jo.getString(KEY_SCORE);
                homeNames[i] = jo.getString(KEY_HOME);
                homeImages[i] = jo.getString(KEY_HIMAGE);
                oponentNames[i] = jo.getString(KEY_OPONENT);
                oponentImages[i] = jo.getString(KEY_OIMAGE);
                tournaments[i] = jo.getString(KEY_TOURNAMENT);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}